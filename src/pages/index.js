import React, { Fragment } from "react"

import '../stylesheet.css'

function generateRows(component, times=8) {
	return Array(times).fill().map((_, i) => <Fragment key={i}>{component}</Fragment>);
}

function generatePawnRows(color) {
	const square = <div className='square'>
		<img className='image' src={`/pieces/pawn_${color}.png`} alt={`${color}-pawn`} />
	</div>

	return <div className='row'>
		{generateRows(square)}
	</div>
}

function generateRankedRows(color) {
	const lowerRanks = ['rook', 'knight', 'bishop']
	const allRanks = [...lowerRanks, 'queen', 'king', ...lowerRanks.reverse()]

	const allRankedComponents = allRanks.map((name, i) => (
		<div className='square' key={i}>
			<img className={`image ${color} ${name}`} src={`/pieces/${name}_${color}.png`} alt={`${color}-${name}`} />
		</div>
	))

	return <div className='row'>
		{allRankedComponents}
	</div>
}

function generateEmptyRows(times) {
	const square = <div className='square'></div>
	const row = <div className='row'>
		{generateRows(square, 8)}
	</div>

	return generateRows(row, times)
}

export default () => {
	return <div className='chess'>
		{generateRankedRows('white')}
		{generatePawnRows('white')}
		{generateEmptyRows(4)}
		{generatePawnRows('black')}
		{generateRankedRows('black')}
	</div>
}
